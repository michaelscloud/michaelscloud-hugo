+++
date = "2019-06-15T21:28:34+01:00"
draft = true
title = "One Wedding..."

+++




# It's a bit of a cliché...

 ...but it's true. And 21 years later it's still true.

Literally she was on the far side of the room when I saw her. It took one look and I was hooked. A small spark or something, that would grow into a wonderful thing that, although it's a bit of a cliché, 21 years later it is still true.

It was a big old dinner. Lots if people I knew. Lots of people I called friends. There were more than a few I had never met before but thats the way of these big celebration dinners.

One of the best things about them are the people you get to eat and drink with who your don't really know. If it's in hotel then so much the better. You don't have to go anywhere afterwards.

Its still a cliché, but 21 years and one wedding later, it's still true.

I'd like to continue with talk of an evening of dancing, mutual attraction and desire but, although there was an introduction, there is no memory of us meeting until breakfast the next day. We have put it down to the ill advised goatee beard that attended dinner but didn't make it to breakfast. Or could have been the Peach Snaps.

I have no idea how to discribe the rest but, although it's a bit of a cliché, 21 years, 1 wedding and a couple of houses later it really is still true.

Some 6 months later we both found ourselves available and in the right place and, well, off we went.

There was a couple of dinners and a false start and an agrement to be friends, becasue it just wasn't going to work. Then one evening, avoiding the sad lonely bastards night out on valentines day, we took a trip to London that ended with a request for a kiss that, although its a cliché, 21 years later, 1 wedding and 2 children later it is still true.

To say it was love at first sight would be a step to far. Im not sure I believe thats possible. To say you love someone means you have to know them and understand them.all their ups and downs. The positives and negatives. All the glorious highs and the terrible lows of a personality. To love is to know every inch of someone, physically, mentally, emotionally and to not wish any of it away. To remove a single part of someone would render them a different person. 

It's a cliché, something that people hope for but few are lucky enough to find.

After 21 years, 1 wedding, 2 children, 2 houses, several jobs, the occasional argument and far to many happy moments to count later, it's still true. I love her like I never thought I could love anyone.
+++
date = "2016-08-28T19:50:00+01:00"
draft = false
title = "Hello World (Via IPv6 only*)"

+++

## *Or "Why I built an IPv6 only website"

![IPv4, Time is running out](/images/IPv6/time.png "IPv4, Time is running out")

<sup>*Time is up - (C) Michael A Daly 2015*</sup> 






## Some Basics

*Everything* on the internet has a unique address. If it's not unique how would you find it? 

It's not that simple but just run with it for a while.

When the Internet (as we understand it today) kicked off we used a protocol called 'IPv4' to create those addresses.

"IPv what?" I hear some of you ask.

Well, if you don't know you probably don’t want (or need) to know but if you do then [Wikipedia has the answers.](https://en.wikipedia.org/wiki/IPv4)

Ipv4 is now considered a "Legacy Protocol". 

> *"Why?"*




### It's a maths thing. 


Because of the way IPv4 is constructed with 32 binary bits, you *only* have 4,294,967,296 unique addresses.

And it has run out.

Yep, all 4 billion addresses have been used up.

There are lots of dirty tricks we have using for years to conserve and recover the space but it's all gone.

There is no more.

Back in the early 1990’s engineers began work on an updated protocol to fix exactly this issue and IPv6 was ratified around 1996.

(IPv5? - It's a long story. Why don't you ask [Wikipedia](https://en.wikipedia.org/wiki/IPv5))

![IPv4, This train has left the station](/images/IPv6/ipv4 has left the station2.jpg "IPv4, This train has left the station")

<sup>*We are sorry to announce that the IPv4 train has left the station- (C) Michael A Daly 2016*</sup>


> *"How is IPv6 Better?"*

### It's a maths thing. 

There are 128 bits to an IPv6 address so you have 3.4×10<sup>38</sup> addresses. Its a long number.

Lots of space for you, me, the dog, all our devices, the fridge, the washing machine and the cat.

And the hamster.


![IPv6, there really is a lot of space](/images/IPv6/lotsofspace2.jpg "IPv6, there really is a lot of space out here")

<sup>*IPv6, there realy is a lot of space out here- (C) Michael A Daly 2016*</sup>

Want to know more? - You guessed it, [Wikipedia has even more answers](https://en.wikipedia.org/wiki/IPv6)


Google have been [tracking the adoption of IPv6](https://www.google.com/intl/en/ipv6/statistics.html) which shows its getting better. But 14% is just not good enough.


## How am I going to help?


Starting with this site, I'm going to run everying I can over IPv6 only.

My initial goal was just to rebuild my (this) web site , because I was finding Wordpress just a little frustrating and dull to use.

I wanted it to have IPv6 and SSL all the way though the stack to make everything nice and secure, not because anything there is particularly sensitive, but there is not reason not to secure and encrypt all your web traffic. 

No matter what your governments tell you. 

I also wanted to have several sites running on one server as Littlest Cloud is formulating a blog as an outpouring for his obsession with cars - and you never know what else might be useful.

> *"All sounds easy so far, but why only IPv6?"*

Well, partly because it was a way to solve the problem of having multipule sites on one server, Partly becasue I could and but mostly to prove that no one has any excuse not to.

And I'm just geeky enough to think this was rather neat.


> *"How?"*

First I should make something clear.

Yes I work for a tech company, and I understand (mostly) how the Internet is all plumbed together and what's needed to make it work.

*But* I am not a developer & I spend very litte time with my hands inside servers or code. At least not any more. 

Like most people in my position I serverd my apprentiship at the coal face of IT, but I got fed up of crawling around cages and racking servers so started to focus on the people and the processes that make those teams work.

So in getting this project running I had a head start, and a few nudges in the right direction from a colleague, [@jogbert,](https://twitter.com/jogbert) (look him up),
but it's mostly my own work.

And it really wasn't that difficult.

I built a virtual private server in [DigitalOcean](www.digitalocean.com), remembering to tick the "I want IPv6" box. If you are new to this then remember to secure your server they come out of the box quite defenceless.


Unfortuntaly this is where the grand plan falls down a bit.

You have to go back and configure the IPv6 address after the server is built as it only boots with IPv4. Also, when at home, I have to use IPv4 to access the server because my ISP 
won't give me an IPv6 address. So I have had to keep an IPv4 route back to the server. IPv6 works very nicely at the office, but only working on this at work would be difficult.

Installing and configuring a web server is straight forward ([Google](https://www.google.co.uk) is your friend here). I chose [NGINX](https://www.nginx.com), but there are lots out there.

And then on to the webpage software.

Like most internet technologies, there are lots of software options out there. I went for Hugo on the recomendation of another colleague [@tenzer](https://twitter.com/tenzer), (you should look him up too).


[Hugo](https://gohugo.io) is a static content generatior - It takes your code and builds static pages (i.e the user can't interact with them) -  it's very lightweight & easy to setup with good documentation. It took a bit of tweeking to customise, but it was a lot less hassel than I expetced. 

And there we have it. My Website. IPv6


# "I still dont get it..."


I can hear one of my friends asking right now..

> *"If I don’t have IPv6 because my ISP wont give it to me, how do I see your supposedly IPv6 only website"*

This is where [CloudFlare](https://www.cloudflare.com) come in. 

They (well, we beacuse I am part of the CloudFlare team) know that the world can't cope with an IPv6 only world right now. 

One of the services CloudFlare run is a web proxy. That means we stand at the edge of the internet and tell the world my site is available on their network.

Setting up [CloudFlare](https://www.cloudflare.com) is actually the easiest part. If you have got as far as buying & registering a domain name , building a server & cofiguring nginx CloudFlare is really easy. Little cloud did his own. He's 11. (sorry.... "not quite 12!")

1. Go to [www.cloudFlare.com](https://www.cloudflare.com)
2. Set up an account
3. Follow the [setup guide.](https://support.cloudflare.com/hc/en-us/categories/200275218) 


My dns page looks like this


![CloudFlare DNS Settings](/images/IPv6/cfdns.png)



CloudFlare take the settings above and publish their own IPv6 and IPv4 addresses for your site into DNS. 

We take care of proxying traffic from the legacy protocol to shiney new IPv6. We are nice like that. 


The flow goes something like this.

 - CloudFlare receive your requests to see this website.

 - CloudFlare fetch the content from my DigitalOcean server on my ipv6 address.

 - CloudFlare send it back to you using whatever IP version you asked for it on.


> *"Yes yes, CloudFlare help but again why?"*


IPv4 is Legacy.

It's old and out of date. 

Its run out.

There is no more.

It's a dead Norwegian Blue.

We have to move to IPv6 or the internet will become more difficult to traverse and less stable.

It takes just a little bit of effort to get the setups right and make it all work.

And if I can do it, there is no reason why every Tom, Dick and Harry with .com, .uk or .whatever can't.






## Guides

[Building & Securing DigitalOcean Servers](https://www.digitalocean.com/help/getting-started/setting-up-your-server/)


[How to enable ipv6 for you DigitalOcean droplet](https://www.digitalocean.com/community/tutorials/how-to-enable-ipv6-for-digitalocean-droplets)


[HUGO](https://gohugo.io/)


[CloudFlare](https://www.cloudflare.com/)

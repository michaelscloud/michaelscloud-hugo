+++
date = "2018-03-05T19:02:26Z"
draft = false 
title = "Reshaping the Band"

+++
### How do you build a new team, from an old team, without screwing it up?

> Have you heard? We're putting the band back together!

I love that phrase. I hate that phrase. Musically speaking it can be exciting. Former band mates getting together again can produce something quite remarkable. Then you remember what some reunion albums have been like.

It's used a lot when people are putting teams that used to work together back together. Which sounds great, but skips over the reason why they aren't a team anymore.

I'm currently trying to build some new teams out of existing teams. It's not so much putting the band back together as taking it apart and re-assembling it. Hoping we don't lose Don Henley in the process.

Your team is all important. It's the thing that enables everything, *everything,* to happen in your organisation. You should be taking as much care are attention to detail over your team as your product. So taking it to pieces and reassembling it is just as tough as rebranding your website.


![Its a team effort. From the ground up.](/images/putingtheband/ittakesateam_3.png "Its a team effort. From the ground up")

<sup>*Its a team effort. From the ground up - (C) Irwin Fule-Ver 2017*</sup>

I'm asking the guitarist to play bass, the bass player to play drums. I'd quite like the drummer to play percussion, but she wants to be a singer, which is hard. She can sing. But we shouldn't let her do it in public right now. 

> How does she feel about that?

Well, not great, but we can work with it.

Our aspiring singer, lets call her Lucy, knows that we have 3 singers, and thats all we need. 3 gives us the range we need and the split harmonies work perfectly well. We also know that one of the singers want to play keyboards, but we can't justify a 2nd right now, we need 3 strong singers.

So we agree to help Lucy improve her singing and help her learn all the songs so, when the time is right, (when we get that stadium tour backing Gary Barlow), she can slip into the role without any trouble.

The horn section? - They are very happy where they are, thanks for asking.

> Ok, whats the lesson?

I have been through many restructures before and they are always painful. But they are more painful when you don't tell anyone what's going on. They are more painful if you don't give people the opportunity to talk about how they feel. They are particularly painful on your team if you don't give them the opportunity to try something new.

We could just tell Lucy that it's not an option and leave it there. But that's very short sighted. It's easy to underestimate what you do to someone's self esteem when you tell them you aren't going to let them do the job they want and believe they can do. We need Lucy because she is a great drummer and a first class glockenspiel player. Loosing her to another band would really hurt. Not only does she know the songs, she's great at keeping the rest of the band in time. So lets work with her, over time, so when the position comes up she is the first in line for the role.

![Your Product is only as good as the team holidng it together.](/images/putingtheband/ittakesateam.png "[Your Product is only as good as the team holidng it together.")

<sup>*Your Product is only as good as the team holidng it together - (C) Irwin Fule-Ver 2017*</sup>

### Let's not torture that metaphore any more.

Building teams is hard. Particularly when you have to deal with multiple locations and time zones. It's more important than you can imagine that you take the time to do it right. Invest in your recruitment processes, take time to pick the candidates and make sure you set people up for success. Never hire anyone with the thought that you can fire them if it doesn't work out.

It's even more important that having built your team to a scale where it doesn't work any more you reshape it with even more care and delicacy. Talk, consider, consult. Set realistic time lines and tell people what they are. Set expectations.

> So how do you How do you build a new team, from an old team, without screwing it up?

Be open.  
Be honest.  
Be realistic.  
Never take your team for granted.  
Never assume they don't have better ideas than you.   
Never assume that they have more to lose than you do.

Do this, and there is less chance that your star drummer will be on tour with Maroon 5 instead of you.